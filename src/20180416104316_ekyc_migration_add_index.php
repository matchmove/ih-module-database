<?php

use Phinx\Migration\AbstractMigration;

class EkycMigrationAddIndex extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('ekyc_migration');

        $userField = $table->hasColumn('user_id');
        $userIndex = $table->hasIndex('USER_IDX');

        if($userField && !$userIndex){
            $table->addIndex(array('user_id'), array('name' => 'USER_IDX'))
                    ->update();
        }

        $statusField = $table->hasColumn('status');
        $statusIndex = $table->hasIndex('STATUS_IDX');

        if($statusField && !$statusIndex){
            $table->addIndex(array('status'), array('name' => 'STATUS_IDX'))
                    ->update();
        }
    }

    public function down()
    {
        $table = $this->table('ekyc_migration');

        $table->removeIndexByName('USER_IDX');

        $table->removeIndexByName('STATUS_IDX');
    }
}
