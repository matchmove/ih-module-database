<?php

use Phinx\Migration\AbstractMigration;

class AlterCardOrderDetailsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('card_order_details');

        $column = $table->hasColumn('weight');
        if ($column) {
            $table->changeColumn('weight', 'decimal', ['precision' => 10, 'scale' => 2, 'null' => true])
                    ->update();
        }
        
        $column = $table->hasColumn('declared_value');
        if ($column) {
            $table->changeColumn('declared_value', 'decimal', ['precision' => 10, 'scale' => 2, 'null' => true])
                    ->update();
        }
                
        $column = $table->hasColumn('collectable_value');
        if ($column) {
            $table->changeColumn('collectable_value', 'decimal', ['precision' => 10, 'scale' => 2, 'null' => true])
                    ->update();
        }
                        
        $column = $table->hasColumn('length_cms');
        if ($column) {
            $table->changeColumn('length_cms', 'decimal', ['precision' => 10, 'scale' => 2, 'null' => true])
                    ->update();
        }
                                
        $column = $table->hasColumn('breadth_cms');
        if ($column) {
            $table->changeColumn('breadth_cms', 'decimal', ['precision' => 10, 'scale' => 2, 'null' => true])
                    ->update();
        }
                                
        $column = $table->hasColumn('height_cms');
        if ($column) {
            $table->changeColumn('height_cms', 'decimal', ['precision' => 10, 'scale' => 2, 'null' => true])
                    ->update();
        }
        
        $column = $table->hasColumn('pieces');
        if ($column) {
            $table->changeColumn('pieces', 'integer', ['null' => true])
                    ->update();
        }
    }
}
