<?php

use Phinx\Migration\AbstractMigration;

class CreateCardOrderDetailsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('card_order_details', ['id' => false, 'primary_key' => 'id']);

        $table
            ->addColumn('id', 'biginteger', ['identity' => true])
            ->addColumn('order_id', 'biginteger', ['null' => true])
            ->addColumn('airway_bill', 'string', ['null' => true, 'limit' => 45])
            ->addColumn('reference_number', 'string', ['null' => true, 'limit' => 45])
            ->addColumn('sender_store_name', 'string', ['null' => true, 'limit' => 150])
            ->addColumn('attention', 'string', ['null' => true, 'limit' => 150])
            ->addColumn('address_1', 'text', ['null' => true])
            ->addColumn('address_2', 'text', ['null' => true])
            ->addColumn('address_3', 'text', ['null' => true])
            ->addColumn('pincode', 'string', ['null' => true, 'limit' => 20])
            ->addColumn('tel_number', 'string', ['null' => true, 'limit' => 45])
            ->addColumn('mobile', 'string', ['null' => true, 'limit' => 14])
            ->addColumn('prod_sku_code', 'string', ['null' => true, 'limit' => 45])
            ->addColumn('contents', 'string', ['null' => true, 'limit' => 45])
            ->addColumn('weight', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('declared_value', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('collectable_value', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('vendor_code', 'string', ['null' => true, 'limit' => 150])
            ->addColumn('shipper_name', 'string', ['null' => true, 'limit' => 45])
            ->addColumn('return_address_1', 'text', ['null' => true])
            ->addColumn('return_address_2', 'text', ['null' => true])
            ->addColumn('return_address_3', 'text', ['null' => true])
            ->addColumn('return_pin', 'string', ['null' => true, 'limit' => 45])
            ->addColumn('length_cms', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('breadth_cms', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('height_cms', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('pieces', 'integer', ['null' => false])
            ->addColumn('area_customer_code', 'string', ['null' => true, 'limit' => 45])
            ->addColumn('date_added', 'datetime', ['null' => true])
            ->create();
    }
}
