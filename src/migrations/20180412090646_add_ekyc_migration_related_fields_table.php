<?php

use Phinx\Migration\AbstractMigration;

class AddEkycMigrationRelatedFieldsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('ekyc_migration');
        $table->addColumn('user_id', 'biginteger')
            ->addColumn('status', 'enum', array('null' => true, 'values'=>'user,address,document,image', 'default' => null))
            ->addColumn('date_migrated', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->create();

        $table = $this->table('users');

        $column = $table->hasColumn('ekyc_migrated');
        if ($column) {
            $table->removeColumn('ekyc_migrated');
        }
    }
}
