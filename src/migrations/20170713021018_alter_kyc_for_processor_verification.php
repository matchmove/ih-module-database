<?php

use Phinx\Migration\AbstractMigration;

class AlterKycForProcessorVerification extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('kyc');
        $table->changeColumn('for_processor_verification', 'enum', array('null' => true, 'values'=>'pending registration submission,submitted to ycs,information incomplete,request face to face verification,face to face verification approved,face to face verification rejected,face to face KYC not required,Rejected', 'default' => 'pending registration submission'))
            ->save();

    }

    public function down()
    {
        $table = $this->table('kyc');
        $table->changeColumn('for_processor_verification', 'enum', array('null' => true, 'values'=>'pending registration submission,submitted to ycs,information incomplete,request face to face verification,face to face verification approved,face to face verification rejected', 'default' => 'pending registration submission'))
            ->save();

    }
}
