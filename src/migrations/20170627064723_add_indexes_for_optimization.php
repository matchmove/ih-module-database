<?php

use Phinx\Migration\AbstractMigration;

class AddIndexesForOptimization extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $exists = $this->hasTable('users');
        if($exists) {
            $table = $this->table('users');
            $table->addIndex('email', array('name' => 'EMAIL_IDX'))
                ->addIndex('hash_id', array('name' => 'HASH_ID_IDX'))
                ->update();
        }

        $exists = $this->hasTable('user_details');
        if($exists) {
            $table = $this->table('user_details');

            //remove the existing indexes of user_details related to user_id
            if($table->hasIndex('user_id')) {
                $table->removeIndexByName('user_details_user_id');
                $table->removeIndexByName('user_id');
            }

            $table->addIndex('user_id', array('name' => 'USER_ID_IDX'))
                ->update();
        }

        $exists = $this->hasTable('card_transactions');
        if($exists) {
            $table = $this->table('card_transactions');
            $table->addIndex(array('primary_account_number', 'transaction_unique_id'), array('name' => 'PAN_TXN_INDX'))
                ->update();
        }

        $exists = $this->hasTable('cards');
        if($exists) {
            $table = $this->table('cards');

            $column1 = $table->hasColumn('card_number');
            $column2 = $table->hasColumn('transaction_unique_id');
            if ($column1 && $column2) {
                $table->addIndex(array('card_number', 'transaction_unique_id'), array('name' => 'CN_TXN_INDX'))
                    ->update();
            }

            $table->addIndex('hash_id', array('name' => 'HASH_ID_IDX'))
                ->addIndex('user_id', array('name' => 'USER_ID_IDX'))
                ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $exists = $this->hasTable('users');
        if($exists) {
            $table = $this->table('users');
            $table->removeIndexByName('EMAIL_IDX');
            $table->removeIndexByName('HASH_ID_IDX');
        }

        $exists = $this->hasTable('user_details');
        if($exists) {
            $table = $this->table('user_details');
            $table->removeIndexByName('USER_ID_IDX');

            $table->addIndex('user_id')
                ->update();
        }

        $exists = $this->hasTable('card_transactions');
        if($exists) {
            $table = $this->table('card_transactions');
            $table->removeIndexByName('PAN_TXN_INDX');
        }

        $exists = $this->hasTable('cards');
        if($exists) {
            $table = $this->table('cards');
            $table->removeIndexByName('CN_TXN_INDX');
            $table->removeIndexByName('HASH_ID_IDX');
            $table->removeIndexByName('USER_ID_IDX');
        }
    }
}