<?php

use Phinx\Migration\AbstractMigration;

class AddFieldsToCardTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('cards');

        $column = $table->hasColumn('number');
        if (!$column) {
            $table->addColumn('number', 'string', array('after'=>'user_id', 'null' => true))
                  ->update();
        }

        $column = $table->hasColumn('proxy_number');
        if (!$column) {
            $table->addColumn('proxy_number', 'string', array('after'=>'number', 'null' => true))
                  ->update();
        }
    }
}
