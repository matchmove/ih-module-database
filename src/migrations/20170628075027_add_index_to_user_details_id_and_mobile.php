<?php

use Phinx\Migration\AbstractMigration;

class AddIndexToUserDetailsIdAndMobile extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    public function change()
    {
        $exists = $this->hasTable('user_details');
        if($exists) {
            $table = $this->table('user_details');
            $table->addIndex(array('id_type', 'id_number'), array('name' => 'ID_NUMBER_IDX'))
                ->addIndex(array('mobile_country_code', 'mobile'), array('name' => 'MOBILE_IDX'))
                ->update();
        }
    }
    
    /**
     * Migrate Down.
     */
    public function down()
    {
        $exists = $this->hasTable('user_details');
        if($exists) {
            $table = $this->table('user_details');
            if($table->hasIndex('ID_NUMBER_IDX')) $table->removeIndexByName('ID_NUMBER_IDX');
            if($table->hasIndex('MOBILE_IDX')) $table->removeIndexByName('MOBILE_IDX');
        }
    }
}
