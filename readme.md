# IRONHIDE MODULES / DATABASE README #

This is part of IRONHIDE Module.

### composer.json ###

```
{
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "ih-database",
                "version": "dev-master",
                "source": {
                    "url": "https://bitbucket.org/matchmove/ih-module-database",
                    "type": "git",
                    "reference": "master"
                }
            }
        }
    ],
    "require": {
        "robmorgan/phinx": "dev-0.7.x-dev",
        "ih-database": "dev-master"
    }
}
```

### phinx.php ###

```
return array(
    "paths" => array(
        "migrations" => '{modules/ih-database/src/migrations,db/migrations}',
        "seeds" => "db/seeds",
    )
    ...
```

### Phinx create ###

```
modules/bin/phinx create CreateTable --path modules/ih-database/src/migrations/
```

### Who do I talk to? ###

* IRONHIDE Team
* MatchMove Engineers